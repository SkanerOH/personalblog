﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepository<E>
    {
        Task<IEnumerable<E>> GetAllAsync();

        Task<E> GetByIdAsync(int id);

        Task AddAsync(E entity);

        void Update(E entity);

        Task DeleteByIdAsync(int id);

    }
}
