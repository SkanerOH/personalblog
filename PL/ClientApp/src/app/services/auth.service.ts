import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Constants } from '../Helpers/Constants';
import { ResponseModel } from '../Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from '../Models/ResponseModels/ResponseModelCode';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  login(email:string, password: string) : Observable<ResponseModel>
  {
    const body={
      Email:email,
      Password:password
    }
    return this.httpClient.post(environment.baseURL+"Auth/login", body).pipe(map((res : any) => {
      var mappedRes = new ResponseModel(res.responseCode, res.responseMessage, res.data);
      // console.log(mappedRes.responseMessage);
      if (mappedRes.responseCode==ResponseModelCode.OK)
      {
        localStorage.setItem(Constants.authUserKey, JSON.stringify(mappedRes.data));
      }
      return mappedRes;
    }));
  }

  register(email:string, password: string, fullname: string) : Observable<ResponseModel>
  {
    const body={
      Email:email,
      Password:password,
      FullName:fullname
    }
    return this.httpClient.post(environment.baseURL+"Auth/register", body).pipe(map((res : any) => {
      var mappedRes = new ResponseModel(res.responseCode, res.responseMessage, res.data);
      // console.log(mappedRes.responseMessage);
      if (mappedRes.responseCode==ResponseModelCode.OK)
      {
        localStorage.setItem(Constants.authUserKey, JSON.stringify(mappedRes.data));
      }
      return mappedRes;
    }));
  }

  changePassword(oldPassword : string, newPassword : string, confirmPassword : string) : Observable<ResponseModel>
  {
    const body={
      Password : oldPassword,
      NewPassword : newPassword,
      ConfirmPassword : confirmPassword
    }

    return this.httpClient.post<ResponseModel>(environment.baseURL+"Accounts/changepassword", body).pipe(map((res : ResponseModel) =>{
      if (res.responseCode==ResponseModelCode.OK)
      {
        this.logout();
      }
      return res;
    }));
  }

  logout()
  {
    localStorage.removeItem(Constants.authUserKey);
  }

  isAuthorized() : boolean
  {
    const user = JSON.parse(localStorage.getItem(Constants.authUserKey));
    if (user && user.token.length > 0)
      {
        return true;
      }
    return false;
  }

  isAdmin() : boolean
  {
    const user = JSON.parse(localStorage.getItem(Constants.authUserKey));
    if (user)
    {
      return user.role == 'admin';
    }
    return false;
  }

  getCurrentUserName() : string
  {
    const user = JSON.parse(localStorage.getItem(Constants.authUserKey));
    if (user)
    {
      return user.fullName;
    }
    return "";
  }

  getCurrentUserRole() : string
  {
    const user = JSON.parse(localStorage.getItem(Constants.authUserKey));
    if (user)
    {
      return user.role;
    }
    return "";
  }

  getCurrentUserId() : string
  {
    const user = JSON.parse(localStorage.getItem(Constants.authUserKey));
    if (user)
    {
      return user.id;
    }
    return "";
  }

  getToken() : string
  {
    return JSON.parse(localStorage[Constants.authUserKey]).token;
  }
}
