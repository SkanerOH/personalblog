import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Constants } from '../Helpers/Constants';
import { ResponseModel } from '../Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from '../Models/ResponseModels/ResponseModelCode';

@Injectable({
  providedIn: 'root'
})
export class BlogsService {

  constructor(private httpClient: HttpClient) { }

  getAllBlogs() : Observable<ResponseModel> 
  {
      return this.httpClient.get<ResponseModel>(environment.baseURL+"blogs");
  }

  updateBlog(title : string, description : string, blogId : number) : Observable<ResponseModel>
  {
    const body={
      Title:title,
      Description : description
    }
    return this.httpClient.put(environment.baseURL+`blogs/${blogId}`, body).pipe(map((res : any) => {
      var mappedRes = new ResponseModel(res.responseCode, res.responseMessage, res.data);
      return mappedRes;
    }));
  }

  deleteBlog(blogId : number) : Observable<ResponseModel>
  {
    return this.httpClient.delete(environment.baseURL+`blogs/${blogId}`).pipe(map((res : any) => {
      var mappedRes = new ResponseModel(res.responseCode, res.responseMessage, res.data);
      return mappedRes;
    }));
  }

  banBlog(blogId: number) : Observable<ResponseModel>
  {
    return this.httpClient.patch(environment.baseURL+`blogs/${blogId}/ban`, {}).pipe(map((res : any) => {
      var mappedRes = new ResponseModel(res.responseCode, res.responseMessage, res.data);
      return mappedRes;
    }));
  }

  unbanBlog(blogId: number) : Observable<ResponseModel>
  {
    return this.httpClient.patch(environment.baseURL+`blogs/${blogId}/unban`, {}).pipe(map((res : any) => {
      var mappedRes = new ResponseModel(res.responseCode, res.responseMessage, res.data);
      return mappedRes;
    }));
  }

  createBlog(title : string, description : string) : Observable<ResponseModel>
  {
    const body={
      Title:title,
      Description : description
    }
    return this.httpClient.post(environment.baseURL+`blogs`, body).pipe(map((res : any) => {
      var mappedRes = new ResponseModel(res.responseCode, res.responseMessage, res.data);
      return mappedRes;
    }));
  }

  getBlogById(blogId : number) : Observable<ResponseModel>
  {
    return this.httpClient.get<ResponseModel>(environment.baseURL+`blogs/${blogId}`);
  }

  getCurrentUserBlogs() : Observable<ResponseModel>
  {
    return this.httpClient.get<ResponseModel>(environment.baseURL+`user/blogs`);
  }
}