import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../Models/ResponseModels/ResponseModel';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private httpClient: HttpClient) { }

  getCommentsByArticleId(blogId : number, articleId : number) : Observable<ResponseModel>
  {
    return this.httpClient.get<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/comments`);
  }

  createComment(blogId : number, articleId : number, text : string) : Observable<ResponseModel>
  {
    const body={
      Text : text
    }
    return this.httpClient.post<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/comments`, body);
  }

  deleteComment(blogId : number, articleId : number, commentId : number) : Observable<ResponseModel>
  {
    return this.httpClient.delete<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/comments/${commentId}`);
  }

  getCommentById(blogId : number, articleId : number, commentId : number) : Observable<ResponseModel>
  {
    return this.httpClient.get<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/comments/${commentId}`);
  }

  updateComment(blogId : number, articleId : number, commentId : number, text : string)
  {
    const body={
      Text : text
    }
    return this.httpClient.put<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/comments/${commentId}`, body);
  }

  banComment(blogId : number, articleId : number, commentId : number)
  {
    return this.httpClient.patch<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/comments/${commentId}/ban`, []);
  }

  unbanComment(blogId : number, articleId : number, commentId : number)
  {
    return this.httpClient.patch<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/comments/${commentId}/unban`, []);
  }
}

