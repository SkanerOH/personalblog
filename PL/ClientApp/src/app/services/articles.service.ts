import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ResponseModel } from '../Models/ResponseModels/ResponseModel';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private httpClient: HttpClient) { }

  getArticlesByBlogId(blogId : number) : Observable<ResponseModel>
  {
    return this.httpClient.get<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles`);
  }

  updateArticle(title : string, text : string, tags : string[], blogId : number, articleId : number) : Observable<ResponseModel>
  {
    const body={
      Title:title,
      Text : text,
      Tags : tags
    }
    return this.httpClient.put<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}`, body);
  }

  deleteArticle(blogId : number, articleId : number) : Observable<ResponseModel>
  {
    return this.httpClient.delete<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}`);
  }

  getArticleById(blogId : number, articleId : number) : Observable<ResponseModel>
  {
    return this.httpClient.get<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}`);
  }

  createArticle(title : string, text : string, tags : string[], blogId : number) : Observable<ResponseModel>
  {
    const body={
      Title:title,
      Text : text,
      Tags : tags
    }
    return this.httpClient.post<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles`, body);
  }

  banArticle(blogId : number, articleId : number) : Observable<ResponseModel>
  {
    return this.httpClient.patch<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/ban`, {});
  }

  unbanArticle(blogId : number, articleId : number) : Observable<ResponseModel>
  {
    return this.httpClient.patch<ResponseModel>(environment.baseURL+`blogs/${blogId}/articles/${articleId}/unban`, {});
  }

  searchArticles(text : string, tags : string[], takenIds : number[]) : Observable<ResponseModel>
  {
    let params = new HttpParams();
    if (text.length>0)
    {
      params = params.append('S', text);
    }
    tags.forEach(tag => {
      params = params.append('T', tag);
    });
    takenIds.forEach(id =>{
      params = params.append('I', id.toString());
    })

    return this.httpClient.get<ResponseModel>(environment.baseURL+`articles/search`, {params : params});
  }
}
