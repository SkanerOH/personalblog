import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ResponseModel } from 'src/app/Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from 'src/app/Models/ResponseModels/ResponseModelCode';
import { CommentVM } from 'src/app/Models/ViewModels/CommentVM';
import { AuthService } from 'src/app/services/auth.service';
import { CommentsService } from 'src/app/services/comments.service';

@Component({
  selector: 'app-comments-control',
  templateUrl: './comments-control.component.html',
  styleUrls: ['./comments-control.component.css']
})
export class CommentsControlComponent implements OnInit, OnDestroy {
  private onDestroy = new Subject();

  @Input() blogId : number;
  @Input() elemId : number;
  @Input() articleId : number;

  @Output() onCommentAdded = new EventEmitter();
  @Output() onCommentDeleted = new EventEmitter();


  comments : CommentVM[] = [];
  errors : string[] = [];

  createCommentForm : FormGroup

  constructor(private authService : AuthService, private commentsService : CommentsService, private formBuilder : FormBuilder) { 
    this.createCommentForm = this.formBuilder.group({
      'text': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(1000)])
    });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  ngOnInit() {
    this.loadComments();
  }

  loadComments() {
    this.commentsService.getCommentsByArticleId(this.blogId, this.articleId).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.comments = data.data;
      }
      else{
        this.errors = data.data;
      }
    });
  }

  createComment() {
    const text = this.createCommentForm.get('text').value;
    this.commentsService.createComment(this.blogId, this.articleId, text).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.onCommentAdded.emit();
        this.loadComments();
      }
      else{
        this.errors = data.data;
      }
    });
  }

  removeComment(obj : any) {
    if (obj.index>-1)
    {
      this.comments.splice(obj.index,1);
      if (!obj.isBanned)
        this.onCommentDeleted.emit();
    }
  }

  reloadComment(index : any) {
    if (index>-1)
    {
      let com = this.comments[index];
      this.commentsService.getCommentById(this.blogId, this.articleId, com.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
        if (data.responseCode==ResponseModelCode.OK)
        {
          this.comments[index] = data.data;
        }
        else{
          this.errors = data.data;
        }
      });
    }
  }

}
