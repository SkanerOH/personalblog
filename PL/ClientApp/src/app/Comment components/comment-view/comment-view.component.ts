import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ResponseModel } from 'src/app/Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from 'src/app/Models/ResponseModels/ResponseModelCode';
import { CommentVM } from 'src/app/Models/ViewModels/CommentVM';
import { AuthService } from 'src/app/services/auth.service';
import { CommentsService } from 'src/app/services/comments.service';

@Component({
  selector: 'app-comment-view',
  templateUrl: './comment-view.component.html',
  styleUrls: ['./comment-view.component.css']
})
export class CommentViewComponent implements OnInit, OnDestroy {
  private onDestroy = new Subject();

  @Input() pelemId : number;
  @Input() elemId : number;
  @Input() comment : CommentVM;
  @Input() blogId : number;
  @Input() articleId : number;
  
  @Output() onChanged = new EventEmitter<number>();
  @Output() onDeleted = new EventEmitter<any>();

  updatingForm : FormGroup;
  updatingFormErrors: string[];

  errors : string[] = [];
  deleteErrors : string[] = [];

  constructor(private authService : AuthService, private commentsService : CommentsService, private formBuilder : FormBuilder) { 
    this.updatingForm = this.formBuilder.group({
      'text': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(1000)])
    });
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  ngOnInit() {
    this.reloadUpdatingFormData();
  }

  deleteComment() {
    let isBanned = this.comment.isBanned;
    this.commentsService.deleteComment(this.blogId, this.articleId, this.comment.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`deleteCommentModal_${this.pelemId}_${this.elemId}_Cancel`)
        ref.click();
        this.onDeleted.emit({index : this.elemId, isBanned : isBanned});
      }
      else{
        this.deleteErrors = data.data;
      }
    });
  }

  reloadUpdatingFormData() {
    this.updatingForm.get('text').setValue(this.comment.text);
  }

  updateComment() {
    const text = this.updatingForm.get('text').value;
    this.commentsService.updateComment(this.blogId, this.articleId, this.comment.id, text).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`updateCommentModal_${this.pelemId}_${this.elemId}_Cancel`)
        ref.click();
        this.onChanged.emit(this.elemId);
      }
      else{
        this.updatingFormErrors = data.data;
      }
    });
  }

  banComment() {
    this.commentsService.banComment(this.blogId, this.articleId, this.comment.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.onChanged.emit(this.elemId);
      }
      else{
        this.errors = data.data;
      }
    });
  }

  unbanComment() {
    this.commentsService.unbanComment(this.blogId, this.articleId, this.comment.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.onChanged.emit(this.elemId);
      }
      else{
        this.errors = data.data;
      }
    });
  }

}
