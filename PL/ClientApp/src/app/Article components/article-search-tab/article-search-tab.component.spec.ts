import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleSearchTabComponent } from './article-search-tab.component';

describe('ArticleSearchTabComponent', () => {
  let component: ArticleSearchTabComponent;
  let fixture: ComponentFixture<ArticleSearchTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleSearchTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleSearchTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
