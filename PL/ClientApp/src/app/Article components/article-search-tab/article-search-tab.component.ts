import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ResponseModel } from 'src/app/Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from 'src/app/Models/ResponseModels/ResponseModelCode';
import { ArticleVM } from 'src/app/Models/ViewModels/ArticleVM';
import { ArticlesService } from 'src/app/services/articles.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-article-search-tab',
  templateUrl: './article-search-tab.component.html',
  styleUrls: ['./article-search-tab.component.css']
})
export class ArticleSearchTabComponent implements OnInit, OnDestroy {

  searchForm : FormGroup;
  addTagForm : FormGroup;

  searchTags : string[] = [];
  articles : ArticleVM[] = [];
  errors : string[] = [];

  lastSearchText : string = "";
  lastSearchTags : string[] = [];
  isLoadMoreDiabled : boolean = true;
  isSearchDone : boolean = false;

  private onDestroy = new Subject();

  constructor(private authService : AuthService, private articlesService : ArticlesService, private formBuilder : FormBuilder) { 
    this.searchForm = this.formBuilder.group({
      'text': new FormControl('', [Validators.maxLength(100)])
    });
    this.addTagForm = this.formBuilder.group({
      'tag' : new FormControl('',[Validators.required, Validators.maxLength(100), Validators.minLength(3), Validators.pattern('\\w+')])
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }
  
  addTag() {
    const tag = this.addTagForm.get('tag').value.toLowerCase();
    if (!this.searchTags.includes(tag)){
      this.searchTags.push(tag);
    }
    this.addTagForm.get('tag').setValue('');
  }

  del_tag(elId : number) {
    if (elId>-1) {
      this.searchTags.splice(elId, 1);
    }
  }

  search() {
    let text = this.searchForm.get('text').value;
    this.articlesService.searchArticles(text, this.searchTags, []).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.articles = data.data;
        this.lastSearchText = text;
        this.lastSearchTags = [...this.searchTags];
        this.isSearchDone = true;
        if (this.articles.length>2)
          this.isLoadMoreDiabled = false;
      }
      else
      {
        this.errors = data.data;
      }
    });
  }

  loadMore() {
    let takenIds = [...this.articles].map(a => a.id);
    this.articlesService.searchArticles(this.lastSearchText, this.lastSearchTags, takenIds).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.articles = this.articles.concat(data.data);
        if (data.data.length<3)
          this.isLoadMoreDiabled = true;
      }
      else
      {
        this.errors = data.data;
      }
    });
  }

  reloadArticle(index : any) {
    let art = this.articles[index];
    this.articlesService.getArticleById(art.blogId, art.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.articles[index] = data.data
      }
      else
      {
        this.errors = data.data;
      }
    });
  }

  removeArticle(index : any) {
    if (index>-1)
      this.articles.splice(index,1);
  }

}
