import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ResponseModel } from 'src/app/Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from 'src/app/Models/ResponseModels/ResponseModelCode';
import { ArticleVM } from 'src/app/Models/ViewModels/ArticleVM';
import { ArticlesService } from 'src/app/services/articles.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-article-view',
  templateUrl: './article-view.component.html',
  styleUrls: ['./article-view.component.css'],
  providers: [DatePipe]
})
export class ArticleViewComponent implements OnInit, OnDestroy {
  private onDestroy = new Subject();
  updatingForm : FormGroup;
  updatingFormTags : string[];
  updatingFormErrors: string[];

  updatingAddTagForm: FormGroup;

  deleteErrors: string[];

  @Input() article : ArticleVM;
  @Input() elemId : number;
  @Output() onChanged = new EventEmitter<number>();
  @Output() onDeleted = new EventEmitter<number>();

  showComments : boolean = false;


  constructor(private authService : AuthService, private datePipe : DatePipe, private formBuilder : FormBuilder,
    private articlesService : ArticlesService) { 
    this.updatingForm = this.formBuilder.group({
      'title': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
      'text': new FormControl('', [Validators.maxLength(8000)])
    });
    this.updatingAddTagForm = this.formBuilder.group({
      'tag' : new FormControl('',[Validators.required, Validators.maxLength(100), Validators.minLength(3), Validators.pattern('\\w+')])
    })
  }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  ngOnInit() {
    this.reloadUpdatingFormData();
  }

  updateArticle() {
    let title = this.updatingForm.get('title').value;
    let text = this.updatingForm.get('text').value;
    let tags = this.updatingFormTags;
    this.articlesService.updateArticle(title, text, tags, this.article.blogId, this.article.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`updateArticleModal_${this.elemId}_Cancel`)
        ref.click();
        this.onChanged.emit(this.elemId);
      }
      else
      {
        this.updatingFormErrors = data.data;
      }
    })
  }

  deleteArticle() {
    this.articlesService.deleteArticle(this.article.blogId, this.article.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`deleteArticleModal_${this.elemId}_Cancel`)
        ref.click();
        this.onDeleted.emit(this.elemId);
      }
      else
      {
        this.deleteErrors = data.data;
      }
    });
  }

  del_tag(elId : number) {
    if (elId>-1) {
      this.updatingFormTags.splice(elId, 1);
    }
  }

  reloadUpdatingFormData() {
    this.updatingForm.get('title').setValue(this.article.title);
    this.updatingForm.get('text').setValue(this.article.text);
    this.updatingFormTags = [... this.article.tags];
  }

  addTag() {
    const tag = this.updatingAddTagForm.get('tag').value.toLowerCase();
    if (!this.updatingFormTags.includes(tag)){
      this.updatingFormTags.push(tag);
    }
    this.updatingAddTagForm.get('tag').setValue('');
  }

  banArticle() {
    this.articlesService.banArticle(this.article.blogId, this.article.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.onChanged.emit(this.elemId);
      }
    });
  }

  unbanArticle() {
    this.articlesService.unbanArticle(this.article.blogId, this.article.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.onChanged.emit(this.elemId);
      }
    });
  }

  onCommentAdded() {
    this.article.commentsCount++;
  }

  onCommentDeleted() {
    this.article.commentsCount--;
  }

}
