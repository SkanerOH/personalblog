import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { LoginTabComponent } from './login-tab/login-tab.component';
import { RegisterTabComponent } from './register-tab/register-tab.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { AuthService } from './services/auth.service';
import { BlogsTabComponent } from './Blog components/blogs-tab/blogs-tab.component';
import { BlogViewComponent } from './Blog components/blog-view/blog-view.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { BlogDetailsTabComponent } from './Blog components/blog-details-tab/blog-details-tab.component';
import { ArticleViewComponent } from './Article components/article-view/article-view.component';
import { CommentsControlComponent } from './Comment components/comments-control/comments-control.component';
import { CommentViewComponent } from './Comment components/comment-view/comment-view.component';
import { MyblogsTabComponent } from './Blog components/myblogs-tab/myblogs-tab.component';
import { ArticleSearchTabComponent } from './Article components/article-search-tab/article-search-tab.component';
import { ChangePasswordTabComponent } from './change-password-tab/change-password-tab.component';
import { ServerErrorComponent } from './Error components/server-error/server-error.component';
import { ServerErrorInterceptor } from './interceptors/server-error.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    LoginTabComponent,
    RegisterTabComponent,
    BlogsTabComponent,
    BlogViewComponent,
    BlogDetailsTabComponent,
    ArticleViewComponent,
    CommentsControlComponent,
    CommentViewComponent,
    MyblogsTabComponent,
    ArticleSearchTabComponent,
    ChangePasswordTabComponent,
    ServerErrorComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'myblogs', pathMatch: 'full' },
      {path: 'login', component: LoginTabComponent},
      {path: 'register', component: RegisterTabComponent},
      {path: 'blogs', component: BlogsTabComponent, canActivate:[AuthGuardService]},
      {path: 'myblogs', component: MyblogsTabComponent, canActivate:[AuthGuardService]},
      {path: 'blogs/:id', component: BlogDetailsTabComponent, canActivate:[AuthGuardService]},
      {path: 'search', component: ArticleSearchTabComponent, canActivate:[AuthGuardService]},
      {path: 'changepassword', component: ChangePasswordTabComponent, canActivate:[AuthGuardService]},
      {path: '500error', component: ServerErrorComponent},
    ],
    {onSameUrlNavigation: 'reload'})
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: TokenInterceptor
    },
    {
      provide: HTTP_INTERCEPTORS,
      multi: true,
      useClass: ServerErrorInterceptor
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
}

