import { Injectable } from "@angular/core";
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, map } from "rxjs/operators";
import { Router } from "@angular/router";
import { AuthService } from "../services/auth.service";

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {
    constructor(private router: Router, private authService : AuthService){

    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError((error : HttpErrorResponse) => {
                if (error.status == 500)
                {
                    this.router.navigate(["500error"]);
                    return throwError(error.error);
                }
                if (error.status == 401 || error.status == 403)
                {
                    this.authService.logout();
                    this.router.navigate(["login"]);
                    return throwError(error.error);
                }
                return throwError(error.error);
            })
        );
    }
}