import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseModelCode } from '../Models/ResponseModels/ResponseModelCode';
import { ResponseModel} from '../Models/ResponseModels/ResponseModel';
import { AuthService } from '../services/auth.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-login-tab',
  templateUrl: './login-tab.component.html',
  styleUrls: ['./login-tab.component.css']
})
export class LoginTabComponent implements OnInit, OnDestroy {
  form: FormGroup;
  errors: string[];
  private onDestroy = new Subject();

  constructor(private authService:AuthService, private router : Router) {
    this.form = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', Validators.required)
    });
   }

  ngOnInit() {
  }

  login() {
    const email = this.form.get('email').value;
    const password = this.form.get('password').value;
    this.authService.login(email, password).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) =>{
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.router.navigate([""]);
      }
      else
      {
        this.errors = data.data;
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}
