import { DatePipe } from '@angular/common';
import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ResponseModel } from 'src/app/Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from 'src/app/Models/ResponseModels/ResponseModelCode';
import { BlogVM } from 'src/app/Models/ViewModels/BlogVM';
import { AuthService } from 'src/app/services/auth.service';
import { BlogsService } from 'src/app/services/blogs.service';

@Component({
  selector: 'app-blog-view',
  templateUrl: './blog-view.component.html',
  styleUrls: ['./blog-view.component.css'],
  providers: [DatePipe]
})
export class BlogViewComponent implements OnInit, OnDestroy {
  updatingForm: FormGroup;
  updatingFormErrors: string[];
  deleteErrors: string[];
  private onDestroy = new Subject();
  @Input() blog : BlogVM;
  @Input() elemId : number;
  @Output() onChanged = new EventEmitter<number>();
  @Output() onDeleted = new EventEmitter<number>();

  constructor(private datePipe : DatePipe, private blogsService : BlogsService, private formBuilder : FormBuilder, private authService : AuthService, private router: Router) {
    this.updatingForm = this.formBuilder.group({
      'blogTitle': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      'description': new FormControl('', [Validators.maxLength(2000)])
    });
   }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  ngOnInit() {
    this.updatingForm.get('blogTitle').setValue(this.blog.title);
    this.updatingForm.get('description').setValue(this.blog.description);
  }

  update(){
    let title = this.updatingForm.get('blogTitle').value;
    let description = this.updatingForm.get('description').value;
    if (description.length==0)
      description = this.blog.description;
    this.blogsService.updateBlog(title, description, this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`updateModal_${this.elemId}_Cancel`)
        ref.click();
        this.onChanged.emit(this.elemId);
      }
      else
      {
        this.updatingFormErrors = data.data;
      }
    })
  }

  delete(){
    this.blogsService.deleteBlog(this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`deleteModal_${this.elemId}_Cancel`)
        ref.click();
        this.onDeleted.emit(this.elemId);
      }
      else
      {
        this.deleteErrors = data.data;
      }
    });
  }

  ban(){
    this.blogsService.banBlog(this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.onChanged.emit(this.elemId);
      }
    });
  }

  unban(){
    this.blogsService.unbanBlog(this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.onChanged.emit(this.elemId);
      }
    });
  }

  info(){
    this.router.navigate([`blogs/${this.blog.id}`])
  }

}
