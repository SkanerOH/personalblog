import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ResponseModel } from 'src/app/Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from 'src/app/Models/ResponseModels/ResponseModelCode';
import { BlogVM } from 'src/app/Models/ViewModels/BlogVM';
import { AuthService } from 'src/app/services/auth.service';
import { BlogsService } from 'src/app/services/blogs.service';

@Component({
  selector: 'app-myblogs-tab',
  templateUrl: './myblogs-tab.component.html',
  styleUrls: ['./myblogs-tab.component.css']
})
export class MyblogsTabComponent implements OnInit, OnDestroy {
  creationForm: FormGroup;
  blogs : BlogVM[];
  errors : string[];
  private onDestroy = new Subject();

  constructor(private authService : AuthService, private blogsService : BlogsService,  private formBuilder : FormBuilder) {
    this.creationForm = this.formBuilder.group({
      'blogTitle': new FormControl('', [Validators.required ,Validators.minLength(6), Validators.maxLength(50)]),
      'description': new FormControl('', [Validators.maxLength(2000)])
    });
   }

  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  ngOnInit() {
    this.loadBlogs();
  }

  onBlogChanged(elemId : any) {
    let b = this.blogs[elemId];
    this.blogsService.getBlogById(b.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.blogs[elemId] = data.data;
      }
      else{
        this.errors = data.data;
      }
    })
  }

  onBlogDeleted(elemId : any) {
    if (elemId>-1)
      this.blogs.splice(elemId, 1);
  }

  createBlog() {
    let title = this.creationForm.get('blogTitle').value;
    let description = this.creationForm.get('description').value;
    this.blogsService.createBlog(title, description).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById('creation_Cancel_btn')
        ref.click();
        this.loadBlogs();
      }
      else
      {
        this.errors = data.data;
      }
    })
  }

  loadBlogs() {
    this.blogsService.getCurrentUserBlogs().pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.blogs = data.data;
      }
      else{
        this.errors = data.data;
      }
    })
  }
}
