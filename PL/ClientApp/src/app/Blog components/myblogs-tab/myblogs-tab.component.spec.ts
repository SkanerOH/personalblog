import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyblogsTabComponent } from './myblogs-tab.component';

describe('MyblogsTabComponent', () => {
  let component: MyblogsTabComponent;
  let fixture: ComponentFixture<MyblogsTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyblogsTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyblogsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
