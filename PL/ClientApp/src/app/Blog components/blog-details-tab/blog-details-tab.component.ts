import { DatePipe } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ResponseModel } from 'src/app/Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from 'src/app/Models/ResponseModels/ResponseModelCode';
import { ArticleVM } from 'src/app/Models/ViewModels/ArticleVM';
import { BlogVM } from 'src/app/Models/ViewModels/BlogVM';
import { ArticlesService } from 'src/app/services/articles.service';
import { AuthService } from 'src/app/services/auth.service';
import { BlogsService } from 'src/app/services/blogs.service';

@Component({
  selector: 'app-blog-details-tab',
  templateUrl: './blog-details-tab.component.html',
  styleUrls: ['./blog-details-tab.component.css'],
  providers: [DatePipe]
})
export class BlogDetailsTabComponent implements OnInit, OnDestroy {

  blogId : number;
  blog : BlogVM;
  errors : string[];
  updatingForm: FormGroup;
  updatingFormErrors: string[];
  deleteErrors: string[];

  creationArticleForm : FormGroup;
  creationArticleFormTags : string[] = [];
  creationArticleFormErrors: string[];

  creationArticleAddTagForm: FormGroup;
  private onDestroy = new Subject();

  articles : ArticleVM[] = [];

  constructor(private activateRoute: ActivatedRoute, private blogsService: BlogsService, private datePipe : DatePipe,
     private authService: AuthService, private formBuilder : FormBuilder,
     private router : Router, private articlesService : ArticlesService) {
    this.blogId = activateRoute.snapshot.params['id'];

    this.loadBlogData();
    this.loadArticles();

    this.updatingForm = this.formBuilder.group({
      'blogTitle': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]),
      'description': new FormControl('', [Validators.maxLength(2000)])
    });

    this.creationArticleForm = this.formBuilder.group({
      'title': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(100)]),
      'text': new FormControl('', [Validators.maxLength(8000), Validators.required])
    });
    this.creationArticleAddTagForm = this.formBuilder.group({
      'tag' : new FormControl('',[Validators.required, Validators.maxLength(100), Validators.minLength(3), Validators.pattern('\\w+')])
    })
   }
  ngOnDestroy(): void {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

  ngOnInit() {
  }

  loadBlogData() {
    this.blogsService.getBlogById(this.blogId).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.blog = data.data;
        this.loadDataToUpdateForm();
      }
      else
      {
        this.errors = data.data; 
      }
    })
  }

  loadArticles() {
    this.articlesService.getArticlesByBlogId(this.blogId).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.articles = data.data;
      }
      else
      {
        this.errors = data.data; 
      }
    })
  }

  update(){
    let title = this.updatingForm.get('blogTitle').value;
    let description = this.updatingForm.get('description').value;
    if (description.length==0)
      description = this.blog.description;
    this.blogsService.updateBlog(title, description, this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`updateBlogModal_Cancel`)
        ref.click();
        this.loadBlogData();
        this.loadDataToUpdateForm();
      }
      else
      {
        this.updatingFormErrors = data.data;
      }
    })
  }

  delete(){
    this.blogsService.deleteBlog(this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`deleteBlogModal_Cancel`)
        ref.click();
        this.router.navigate(["myblogs"]);
      }
      else
      {
        this.deleteErrors = data.data;
      }
    });
  }

  loadDataToUpdateForm(){
    this.updatingForm.get('blogTitle').setValue(this.blog.title);
    this.updatingForm.get('description').setValue(this.blog.description);
  }

  banBlog(){
    this.blogsService.banBlog(this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.loadBlogData();
      }
    });
  }

  unbanBlog(){
    this.blogsService.unbanBlog(this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.loadBlogData();
      }
    });
  }

  reloadArticle(index : any) {
    let art = this.articles[index];
    let article = this.articlesService.getArticleById(this.blogId, art.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.articles[index] = data.data
      }
      else
      {
        this.errors = data.data;
      }
    });
  }

  removeArticle(index : any) {
    if (index>-1)
      this.articles.splice(index,1);
  }

  del_tag(elId : number) {
    if (elId>-1) {
      this.creationArticleFormTags.splice(elId, 1);
    }
  }

  addTag() {
    const tag = this.creationArticleAddTagForm.get('tag').value.toLowerCase();
    if (!this.creationArticleFormTags.includes(tag)){
      this.creationArticleFormTags.push(tag);
    }
    this.creationArticleAddTagForm.get('tag').setValue('');
  }

  createArticle() {
    let title = this.creationArticleForm.get('title').value;
    let text = this.creationArticleForm.get('text').value;
    let tags = this.creationArticleFormTags;
    this.articlesService.createArticle(title, text, tags, this.blog.id).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) => {
      if (data.responseCode==ResponseModelCode.OK)
      {
        let ref = document.getElementById(`creationArticleModal_Cancel`)
        ref.click();
        this.loadArticles();
      }
      else
      {
        this.creationArticleFormErrors = data.data;
      }
    })
  }

  reloadCreationArticleFormData() {
    this.creationArticleFormTags = [];
  }

}
