import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlogDetailsTabComponent } from './blog-details-tab.component';

describe('BlogDetailsTabComponent', () => {
  let component: BlogDetailsTabComponent;
  let fixture: ComponentFixture<BlogDetailsTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlogDetailsTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlogDetailsTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
