import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MustMatch } from '../Helpers/MustMatch';
import { ResponseModel } from '../Models/ResponseModels/ResponseModel';
import { ResponseModelCode } from '../Models/ResponseModels/ResponseModelCode';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-change-password-tab',
  templateUrl: './change-password-tab.component.html',
  styleUrls: ['./change-password-tab.component.css']
})
export class ChangePasswordTabComponent implements OnInit {
  form: FormGroup;
  errors: string[] = [];
  private onDestroy = new Subject();

  constructor(private authService:AuthService, private router : Router, private formBuilder : FormBuilder) {
    this.form = this.formBuilder.group({
      'oldPassword': new FormControl('', Validators.required),
      'password': new FormControl('', [Validators.required, Validators.minLength(6)]),
      'confirmpassword': new FormControl('', [Validators.required, Validators.minLength(6)])
    },
    {
      validator: MustMatch('password','confirmpassword')
   });
  }

  ngOnInit() {
  }

  changePassword() {
    const oldPassword = this.form.get('oldPassword').value;
    const newPassword = this.form.get('password').value;
    const confirmPassword = this.form.get('confirmpassword').value;
    this.authService.changePassword(oldPassword, newPassword, confirmPassword).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) =>{
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.router.navigate(["login"])
      }
      else
      {
        this.errors = data.data;
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}
