export enum ResponseModelCode {
    NotSet = 0,
    OK = 1,
    Error = 2
}