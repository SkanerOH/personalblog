import { ResponseModelCode } from "./ResponseModelCode"

export class ResponseModel {
    public responseCode : ResponseModelCode=ResponseModelCode.NotSet;
    public responseMessage : string="";
    public data : any;

    constructor(code : ResponseModelCode, message : string, data : any)
    {
        this.responseCode = code,
        this.responseMessage = message,
        this.data = data
    }
}