export class BlogVM {
    public id : number = 0;
    public title : string = "";
    public description : string = "";
    public modifiedAt : string = "";
    public publisherId : string = "";
    public publisherName : string = "";
    public isBanned : boolean = false;
    public articlesCount : number = 0;

    constructor(id : number, title : string, description : string, modifiedAt : string,
        publisherId : string, publisherName : string, isBanner : boolean, articlesCount : number)
        {
            this.id = id;
            this.title = title;
            this.description = description;
            this.modifiedAt = modifiedAt;
            this.publisherId = publisherId;
            this.publisherName = publisherName;
            this.isBanned = isBanner;
            this.articlesCount = articlesCount;
        }
}