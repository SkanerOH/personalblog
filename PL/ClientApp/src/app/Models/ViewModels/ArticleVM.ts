export class ArticleVM {
    public id : number = 0;
    public title : string = "";
    public text : string = "";
    public commentsCount : number = 0;
    public modifiedAt : string = "";
    public blogId : number = 0;
    public blogTitle : string = "";
    public publisherId : string = "";
    public isBanned : boolean = false;
    public tags : string[] = [];

    constructor(id : number, title : string, text : string, commentsCount : number, modifiedAt : string,
        blogId : number, blogTitle : string, publisherId : string, isBanner : boolean, tags : string[])
        {
            this.id = id;
            this.title = title;
            this.text = text;
            this.commentsCount = commentsCount;
            this.modifiedAt = modifiedAt;
            this.blogId = blogId;
            this.blogTitle = blogTitle;
            this.publisherId = publisherId;
            this.isBanned = isBanner;
            this.tags = tags;
        }
}