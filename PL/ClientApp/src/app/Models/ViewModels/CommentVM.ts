export class CommentVM {
    public id : number = 0;
    public text : string = "";
    public modifiedAt : string = "";
    public publisherId : string = "";
    public publisherName : string = "";
    public isBanned : boolean = false;

    constructor(id : number, text : string, modifiedAt : string,
        publisherId : string, publisherName : string, isBanner : boolean)
        {
            this.id = id;
            this.text = text;
            this.modifiedAt = modifiedAt;
            this.publisherId = publisherId;
            this.publisherName = publisherName;
            this.isBanned = isBanner;
        }
}
