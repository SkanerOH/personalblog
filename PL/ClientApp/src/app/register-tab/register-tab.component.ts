import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseModelCode } from '../Models/ResponseModels/ResponseModelCode';
import { ResponseModel} from '../Models/ResponseModels/ResponseModel';
import { AuthService } from '../services/auth.service';
import { MustMatch } from '../Helpers/MustMatch';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-register-tab',
  templateUrl: './register-tab.component.html',
  styleUrls: ['./register-tab.component.css']
})
export class RegisterTabComponent implements OnInit, OnDestroy {
  form: FormGroup;
  errors: string[];
  private onDestroy = new Subject();

  constructor(private authService:AuthService, private router : Router, private formBuilder : FormBuilder) {
    this.form = this.formBuilder.group({
      'email': new FormControl('', [Validators.required, Validators.email]),
      'password': new FormControl('', Validators.required),
      'confirmpassword': new FormControl('', Validators.required),
      'fullname': new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(50)])
    },
    {
      validator: MustMatch('password','confirmpassword')
   });
  }

  ngOnInit() {
  }

  register() {
    const email = this.form.get('email').value;
    const password = this.form.get('password').value;
    const fullname = this.form.get('fullname').value;
    this.authService.register(email, password, fullname).pipe(takeUntil(this.onDestroy)).subscribe((data : ResponseModel) =>{
      if (data.responseCode==ResponseModelCode.OK)
      {
        this.router.navigate([""]);
      }
      else
      {
        this.errors = data.data;
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy.next();
    this.onDestroy.complete();
  }

}
