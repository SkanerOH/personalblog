﻿using AutoMapper;
using PL.Extentions;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonalBlog.Tests
{
    public static class UnitTestHelper
    {
        public static Mapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }
    }
}
