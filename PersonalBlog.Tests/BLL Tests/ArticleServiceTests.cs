﻿using BLL.Interfaces;
using BLL.Models.DataModels;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PersonalBlog.Tests;
using System.Linq.Expressions;
using BLL.Validation;

namespace PersonalBlog.Tests.BLL_Tests
{
    class ArticleServiceTests
    {
        [Test]
        public async Task GetArticlesWithDetailsByBlogIdAsync_ReturnsArticlesVisableForUserOwner()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticlesWithDetailsAsync(It.IsAny<Expression<Func<Article, bool>>>())).Returns(Task.FromResult(blog1articles as IEnumerable<Article>));

            var expected = blog1articleModels;

            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);
            var actualResult = await service.GetArticlesWithDetailsByBlogIdAsync(blogs[0].Id, "abc", "user");

            Assert.That(actualResult, Is.EquivalentTo(expected).Using(new ArticleModelEC()));
        }

        [Test]
        public async Task GetArticlesWithDetailsByBlogIdAsync_ReturnsArticlesVisableForAdmin()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticlesWithDetailsAsync(It.IsAny<Expression<Func<Article, bool>>>())).Returns(Task.FromResult(blog1articles as IEnumerable<Article>));

            var expected = blog1articleModels;

            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);
            var actualResult = await service.GetArticlesWithDetailsByBlogIdAsync(blogs[0].Id, "cba", "admin");

            Assert.That(actualResult, Is.EquivalentTo(expected).Using(new ArticleModelEC()));
        }

        [Test]
        public void GetArticlesWithDetailsByBlogIdAsync_ThrowsExceptionIfBlogDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(2)).Returns(Task.FromResult<Blog>(null));
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticlesWithDetailsAsync(It.IsAny<Expression<Func<Article, bool>>>())).Returns(Task.FromResult(blog1articles as IEnumerable<Article>));

            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.GetArticlesWithDetailsByBlogIdAsync(2, "cba", "admin"));
        }

        [Test]
        public async Task BanArticleByIdAsync_BansArticle()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            await service.BanArticleByIdAsync(articles[0].Id, "cba", "admin");

            mockUnitOfWork.Verify(x => x.ArticleRepository.Update(It.Is<Article>(a => a.Id == articles[0].Id && a.IsBanned == true)));
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void BanArticleByIdAsync_ThrowsExceptionWhenUserDoesNotHavePermission()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.BanArticleByIdAsync(articles[0].Id, "cba", "user"));
        }

        [Test]
        public void BanArticleByIdAsync_ThrowsExceptionWhenArticleDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult<Article>(null));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.BanArticleByIdAsync(articles[0].Id, "cba", "user"));
        }

        [Test]
        public async Task CreateArticleAsync_CreatesNewArticle()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.AddAsync(It.IsAny<Article>()));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            await service.CreateArticleAsync(blogs[0].Id, "abc", articleModels[0]);

            mockUnitOfWork.Verify(x => x.ArticleRepository.AddAsync(It.Is<Article>(a => a.Id == articleModels[0].Id 
            && a.Title.Equals(articleModels[0].Title) 
            && a.Text.Equals(articleModels[0].Text)
            && a.BlogId == articleModels[0].BlogId)));
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void CreateArticleAsync_ThrowsExceptionWhenBlogDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult<Blog>(null));
            mockUnitOfWork.Setup(x => x.ArticleRepository.AddAsync(It.IsAny<Article>()));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.CreateArticleAsync(blogs[0].Id, "abc", articleModels[0]));
        }

        [Test]
        public void CreateArticleAsync_ThrowsExceptionWhenUserDoesNotHavePermissonToCreateArticleInBlog()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult<Blog>(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.AddAsync(It.IsAny<Article>()));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.CreateArticleAsync(blogs[0].Id, "cba", articleModels[0]));
        }

        [Test]
        public async Task DeleteArticleAsync_DeletesArticle()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.DeleteByIdAsync(articles[0].Id));
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            await service.DeleteArticleAsync(articles[0].Id, "abc");

            mockUnitOfWork.Verify(x => x.ArticleRepository.DeleteByIdAsync(articles[0].Id));
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void DeleteArticleAsync_ThrowsExceptionWhenArticleDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.DeleteByIdAsync(articles[0].Id));
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult<Article>(null));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.DeleteArticleAsync(articles[0].Id, "abc"));
        }

        [Test]
        public void DeleteArticleAsync_ThrowsExceptionWhenBlogDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult<Blog>(null));
            mockUnitOfWork.Setup(x => x.ArticleRepository.DeleteByIdAsync(articles[0].Id));
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult<Article>(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.DeleteArticleAsync(articles[0].Id, "abc"));
        }

        [Test]
        public void DeleteArticleAsync_ThrowsExceptionWhenUserDoesNotHavePermission()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult<Blog>(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.DeleteByIdAsync(articles[0].Id));
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult<Article>(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.DeleteArticleAsync(articles[0].Id, "cba"));
        }

        [Test]
        public async Task GetArticleByIdAsync_ReturnsArticle()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticleWithDatailsByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            var expected = articleModels[0];
            var actual = await service.GetArticleByIdAsync(articles[0].Id, "abc", "user");

            Assert.That(actual, Is.EqualTo(expected).Using(new ArticleModelEC()));
        }

        [Test]
        public void GetArticleByIdAsync_ThrowsExceptionWhenArticleDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticleWithDatailsByIdAsync(articles[0].Id)).Returns(Task.FromResult<Article>(null));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.GetArticleByIdAsync(articles[0].Id, "abc", "user"));
        }

        [Test]
        public void GetArticleByIdAsync_ThrowsExceptionWhenUserDoesNotHavePermissions()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticleWithDatailsByIdAsync(articles[0].Id)).Returns(Task.FromResult<Article>(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.GetArticleByIdAsync(articles[0].Id, "cba", "user"));
        }

        [Test]
        public async Task UnbanArticleByIdAsync_BansArticle()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            await service.UnbanArticleByIdAsync(articles[0].Id, "cba", "admin");

            mockUnitOfWork.Verify(x => x.ArticleRepository.Update(It.Is<Article>(a => a.Id == articles[0].Id && a.IsBanned == false)));
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void UnbanArticleByIdAsync_ThrowsExceptionWhenUserDoesNotHavePermission()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.UnbanArticleByIdAsync(articles[0].Id, "cba", "user"));
        }

        [Test]
        public void UnbanArticleByIdAsync_ThrowsExceptionWhenArticleDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetByIdAsync(articles[0].Id)).Returns(Task.FromResult<Article>(null));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.UnbanArticleByIdAsync(articles[0].Id, "cba", "user"));
        }

        [Test]
        public async Task UpdateArticleAsync_UpdatesArticle()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticleWithDatailsByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.Update(It.IsAny<Article>()));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            await service.UpdateArticleAsync(blogs[0].Id, "abc", articleModels[0]);

            mockUnitOfWork.Verify(x => x.ArticleRepository.Update(It.Is<Article>(a => a.Id == articleModels[0].Id
            && a.Title.Equals(articleModels[0].Title)
            && a.Text.Equals(articleModels[0].Text)
            && a.BlogId == articleModels[0].BlogId)));
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }

        [Test]
        public void UpdateArticleAsync_ThrowsExceptionWhenArticleDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticleWithDatailsByIdAsync(articles[0].Id)).Returns(Task.FromResult<Article>(null));
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult<Blog>(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.Update(It.IsAny<Article>()));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.UpdateArticleAsync(blogs[0].Id, "abc", articleModels[0]));
        }

        [Test]
        public void UpdateArticleAsync_ThrowsExceptionWhenBlogDoesNotExist()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticleWithDatailsByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult<Blog>(null));
            mockUnitOfWork.Setup(x => x.ArticleRepository.Update(It.IsAny<Article>()));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.UpdateArticleAsync(blogs[0].Id, "abc", articleModels[0]));
        }

        [Test]
        public void UpdateArticleAsync_ThrowsExceptionWhenUserDoesNotHavePermissonToEditArticleInBlog()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.GetArticleWithDatailsByIdAsync(articles[0].Id)).Returns(Task.FromResult(articles[0]));
            mockUnitOfWork.Setup(x => x.BlogRepository.GetByIdAsync(blogs[0].Id)).Returns(Task.FromResult<Blog>(blogs[0]));
            mockUnitOfWork.Setup(x => x.ArticleRepository.Update(It.IsAny<Article>()));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            Assert.ThrowsAsync<BlogsException>(() => service.CreateArticleAsync(blogs[0].Id, "cba", articleModels[0]));
        }

        [Test]
        public async Task GetArticlesByFiltersAsync_ReturnsCorrextForAdmin()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.SearchArticleWithDetailsByFiltersAndTakeThatNotTakenAsync(It.IsAny<string>(), It.IsAny<List<string>>(), It.IsAny<List<int>>(), It.IsAny<int>(), It.IsAny<Expression<Func<Article, bool>>>()))
                .Returns(Task.FromResult(articles as IEnumerable<Article>));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            var expected = articleModels;
            var actual = await service.GetArticlesByFiltersAsync("", new List<string>(), new List<int>(), 3, "admin");

            Assert.That(actual, Is.EquivalentTo(expected).Using(new ArticleModelEC()));
        }

        [Test]
        public async Task GetArticlesByFiltersAsync_ReturnsCorrextForUser()
        {
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(x => x.ArticleRepository.SearchArticleWithDetailsByFiltersAndTakeThatNotTakenAsync(It.IsAny<string>(), It.IsAny<List<string>>(), It.IsAny<List<int>>(), It.IsAny<int>(), It.IsAny<Expression<Func<Article, bool>>>()))
                .Returns(Task.FromResult(blog1articles as IEnumerable<Article>));
            IArticleService service = new ArticleService(UnitTestHelper.CreateMapperProfile(), mockUnitOfWork.Object);

            var expected = blog1articleModels;
            var actual = await service.GetArticlesByFiltersAsync("", new List<string>(), new List<int>(), 3, "user");

            Assert.That(actual, Is.EquivalentTo(expected).Using(new ArticleModelEC()));
        }

        private static List<Blog> blogs = new List<Blog>
        {
            new Blog { Id=1, Title = "asdsa", UserWithIdentityId="abc", Description = "adasdasdas", ModifiedAt = DateTimeOffset.UtcNow, CreatedAt = DateTimeOffset.UtcNow, IsBanned = false, IsDeleted = false},
            new Blog { Id=2, Title = "asdasd", UserWithIdentityId="cba", Description = "dasdasdas", ModifiedAt = DateTimeOffset.UtcNow, CreatedAt = DateTimeOffset.UtcNow, IsBanned = false, IsDeleted = false}
        };

        private static List<Comment> art1Comments = new List<Comment>
        {
            new Comment { Id=1, ArticleId =1, IsBanned = false, Text ="asdas"}
        };

        private static List<Article> articles = new List<Article>
        {
            new Article { Id = 1, Title = "art1", Text = "sdaasdas", BlogId = 1, CreatedAt = DateTimeOffset.UtcNow, ModifiedAt = DateTimeOffset.UtcNow, IsBanned = false, IsDeleted = false, Blog = blogs[0], Tags = new List<Tag>()},
            new Article { Id = 2, Title = "art2", Text = "sdaasdas", BlogId = 2, CreatedAt = DateTimeOffset.UtcNow, ModifiedAt = DateTimeOffset.UtcNow, IsBanned = true, IsDeleted = false, Blog = blogs[1], Tags = new List<Tag>()},
            new Article { Id = 3, Title = "art3", Text = "sdaasdas", BlogId = 1, CreatedAt = DateTimeOffset.UtcNow, ModifiedAt = DateTimeOffset.UtcNow, IsBanned = false, IsDeleted = false, Blog = blogs[0], Tags = new List<Tag>()},
        };
        private static List<Article> blog1articles = new List<Article>
        {
            articles[0],
            articles[2]
        };
        private static List<Article> blog2articles = new List<Article>
        {
            articles[1]
        };

        private static List<ArticleModel> articleModels= new List<ArticleModel> {
            new ArticleModel { Id = 1, Title = "art1", Text = "sdaasdas", BlogId = 1, CreatedAt = DateTimeOffset.UtcNow, ModifiedAt = DateTimeOffset.UtcNow, IsBanned = false, IsDeleted = false, Tags = new List<TagModel>()},
            new ArticleModel { Id = 2, Title = "art2", Text = "sdaasdas", BlogId = 2, CreatedAt = DateTimeOffset.UtcNow, ModifiedAt = DateTimeOffset.UtcNow, IsBanned = true, IsDeleted = false, Tags = new List<TagModel>()},
            new ArticleModel { Id = 3, Title = "art3", Text = "sdaasdas", BlogId = 1, CreatedAt = DateTimeOffset.UtcNow, ModifiedAt = DateTimeOffset.UtcNow, IsBanned = false, IsDeleted = false, Tags = new List<TagModel>()},
        };
        private static List<ArticleModel> blog1articleModels = new List<ArticleModel>
        {
            articleModels[0],
            articleModels[2]
        };
        private static List<ArticleModel> blog2articleModels = new List<ArticleModel> {
            articleModels[1]
        };
    }
}
