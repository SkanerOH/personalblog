﻿using BLL.Models.DataModels;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace PersonalBlog.Tests
{
    internal class ArticleModelEC : IEqualityComparer<ArticleModel>
    {
        public bool Equals([AllowNull] ArticleModel x, [AllowNull] ArticleModel y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id; 
                //&&
                //x.IsBanned == y.IsBanned &&
                //x.IsDeleted == y.IsDeleted &&
                //x.Text.Equals(y.Text) &&
                //x.Title.Equals(y.Title) &&
                //x.BlogId == y.BlogId;
        }

        public int GetHashCode([DisallowNull] ArticleModel obj)
        {
            return obj.GetHashCode();
        }
    }
}
